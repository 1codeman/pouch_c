try{
	
	
		var toEvaluate= function (operand1,operand2,operator) {
			
			
			if(errors.length === 0){
				
				  if(tempAssignDtype=="int"){ 	
					      operand1.value = parseInt(operand1.value);
					      operand2.value = parseInt(operand2.value);
				     }else{
				     	   operand1.value = parseFloat(operand1.value);
					       operand2.value = parseFloat(operand2.value);
				     }
	
			   if(tempAssignDtype === operand1.type && tempAssignDtype === operand2.type){
			   		if(operator === '+')
			     		stackValue = operand2.value + operand1.value;
					else if(operator === '-')
			     		stackValue = operand2.value - operand1.value;
					else if(operator === '/')
			    		stackValue = operand2.value / operand1.value;
					else if(operator === '%')
			     		stackValue = operand2.value % operand1.value;
			     	else if(operator === '*')
			     		stackValue = operand2.value * operand1.value;
			     	else if(operator === '>'){
			     		stackValue = operand2.value > operand1.value;	
			     		conditionValue = stackValue;
			     	}
			     		
			     	else if(operator === '<'){
			     		
			     		stackValue = operand2.value < operand1.value;	
			     		conditionValue = stackValue;
			     	}
			     		
			     	else if(operator === '>='){
			     		stackValue = operand2.value >= operand1.value;	
			     		conditionValue = stackValue;
			     	}
			     		
			     	else if(operator === '<='){
			     		stackValue = operand2.value <= operand1.value;	
			     		conditionValue = stackValue;
			     	}
			     		
			     	else if(operator === '=='){
			     		stackValue = operand2.value == operand1.value;	
			     		conditionValue = stackValue;
			     	}
			     				
						
				
				
				    if(tempAssignDtype=="int"){
					      stackValue = parseInt(stackValue);
				     }else{
				     	  stackValue = parseFloat(stackValue);
				     	
				     }
				
					pushStack(operand1.type, stackValue);
			   	
			   }else{
			   				
			   			var x = "ERROR: Data Type Mismatched";
			   		      
	 					addError(tokens[i-1].lexeme,tokens[i-1].token,tokens[i-1].lineNumber,x);	
			   }
				
			
				
			}else{
				//do nothing
			}
			
		};
		
		var check= function (type) {
			
			if(errors.length === 0){
			
				 
			   if(tempAssignDtype === type){
			   		//do nothing
			   	
			   }else{
			   
			   			var x = "ERROR: Data Type Mismatched";
			   		      
	 					addError(tokens[i-1].lexeme,tokens[i-1].token,tokens[i-1].lineNumber,x);	
			   }
				
			
				
			}else{
				//do nothing
			}
			
		};
		
		
		var pushStack = function (type,value) {
		
			stack.push({
			type: type,
			value: value
		});
	};
	
	
	var addError = function (lexeme, token,lineNumber,error) {
		errors.push({
			lexeme: lexeme,
			token: token,
			lineNumber: lineNumber,
			error: error
		});
	};
	
	var insertSymbol = function (type,lexeme,value) {
		
		
			symblTbl.push({
			type: type,
			lexeme: lexeme,
			value: value
		});
	};
	
	var updateSymbol = function (value,index) {
		symblTbl[index].value = value;
	};
	
	
	 var searchSymbol = function (key){
	 	
    	var len = symblTbl.length;
        var num;
        found =0;
       for(num=0;num<len;num++){
       	
       		if(symblTbl[num].lexeme === key){
         		index = num;
         		tempDtype = symblTbl[num].type;
           		found = 1;
           		break;
       		}
       
       
       }
       
        	if(found === 1){
        	
        		return true;
        }
        	else{
        		
        		
        		return false;
        }

	 	
	 };
	 
	
	
	
}catch (e){
	
	
	throw errors[0].error + " on line : " +errors[0].lineNumber;
}
