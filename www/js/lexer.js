var token = { //global token value
	
   			POUCH_C : {token: 0, lexeme: "main"},	
  			KW_INT  : {token: 1, lexeme: "int"},
  			KW_DOUBLE  : {token: 2, lexeme: "double"},
  			KW_IF  : {token: 3, lexeme: "if"},
  			KW_ELSE  : {token: 4, lexeme: "else"},	
  			KW_WHILE  : {token: 5, lexeme: "while"},
  			EQUALITY_OP :{token: 6, lexeme: "=="},
  			GTHAN_OP :{token: 7, lexeme: ">"},
  			LTHAN_OP :{token: 8, lexeme: "<"},
  			GTHANEQUAL_OP :{token: 9, lexeme: ">="},
  			LTHANEQUAL_OP :{token: 10, lexeme: "<="},
  			ASSIGN_OP :{token: 11, lexeme: "="},
			PLUS_OP : {token: 12, lexeme: "+"},	
			MINUS_OP :{token: 13, lexeme: "-"},	
			MULT_OP :{token: 14, lexeme: "*"},	
			DIV_OP :{token: 15, lexeme: "/"},
			LEFT_PAREN :{token: 16, lexeme: "("},	
			RIGHT_PAREN :{token: 17, lexeme: ")"},	
			LEFT_BRACE :{token: 18, lexeme: "{"},
			RIGHT_BRACE :{token: 19, lexeme: "}"},		
			SEMICOLON :{token: 20, lexeme: ";"},
			NUM_INT :{token: 21, lexeme: ""},	
			NUM_DOUBLE :{token: 22, lexeme: ""},		
   			ID      : {token: 23, lexeme: ""},	
   			STRING      : {token: 24, lexeme: ""},	
   			ERROR   : {token: 25, lexeme: ""},
   			COMMA   : {token: 26, lexeme: ","},
   			PRINTF   : {token: 27, lexeme: "printf"},
   			DQUOTE : {token: 28, lexeme: '"'},
   			AMPERSAND : {token: 29, lexeme:"&"},
   			SCANF : {token: 30, lexeme: "scanf"},
   			EOF   : {token: 31, lexeme: "EOF"},
   			KW_CHAR   : {token: 32, lexeme: "char"},
   			SQUOTE   : {token: 33, lexeme: "'"},
   			IS_CHAR  : {token: 34, lexeme: ""},
   			MODULO  : {token: 35, lexeme: "%"}
	};

	var tokens = [];


var lex = function (input) {


	tokens = [];
	var keywords = [
	        "main",
    		"int",
    		"double",
   			 "if",
   			 "else",
   			 "while",
   			 "printf",
   			 "scanf",
   			 "char"

		];


	Array.prototype.contains = function(elem)
	{
   		for (var i in this)
   		{
       		if (this[i] == elem) return true;
   		}
   			return false;
	};

	var isSymbol = function (c) { return /[+\-*\/\^%=()><{};,&]/.test(c); },
		isDigit = function (c) { return /[0-9]/.test(c); },
		isWhiteSpace = function (c) { return /\s/.test(c); },
		isString = function (c) { return /[/"]/.test(c);},
		isChar = function (c) { return /[/']/.test(c);},
		isWord = function (c) { return /[a-zA-Z_]\w*[0-9]*/.test(c);};

	var c, i = 0,flag=0,dtype;
	var advance = function () { return c = input[++i]; };
	var back = function () { return c = input[--i]; };
	var addToken = function (lexeme, token,lineNumber) {
		tokens.push({
			lexeme: lexeme,
			token: token,
			lineNumber: lineNumber
		});
	};

	var lineNumber = 1;
	var status = 0;
	while (i < input.length) {
		c = input[i];

		if (status ===1){
			lineNumber++;
			status = 0;
		}


		//check new line
		if (input[i+1]==='\n'){
				status =1;
		}


		if (isWhiteSpace(c)) advance();


		else if (isSymbol(c)) {

			if(c === "="){
				advance();
				if(c === "="){

					addToken(token.EQUALITY_OP.lexeme,token.EQUALITY_OP.token,lineNumber);


				}else{

					addToken(token.ASSIGN_OP.lexeme,token.ASSIGN_OP.token,lineNumber);
					back();
				}
			}else{

				if(c === "+")
					addToken(token.PLUS_OP.lexeme,token.PLUS_OP.token,lineNumber);
				else if(c === "-"){
					addToken(token.MINUS_OP.lexeme,token.MINUS_OP.token,lineNumber);

				}

				else if(c === "*")
					addToken(token.MULT_OP.lexeme,token.MULT_OP.token,lineNumber);
				else if(c === "/")
					addToken(token.DIV_OP.lexeme,token.DIV_OP.token,lineNumber);
				else if(c === "(")
					addToken(token.LEFT_PAREN.lexeme,token.LEFT_PAREN.token,lineNumber);
				else if(c === ")")
					addToken(token.RIGHT_PAREN.lexeme,token.RIGHT_PAREN.token,lineNumber);
				else if(c === "{")
					addToken(token.LEFT_BRACE.lexeme,token.LEFT_BRACE.token,lineNumber);
				else if( c=== "}")
					addToken(token.RIGHT_BRACE.lexeme,token.RIGHT_BRACE.token,lineNumber);
				else if( c === ";")
					addToken(token.SEMICOLON.lexeme,token.SEMICOLON.token,lineNumber);
				else if( c === ",")
					addToken(token.COMMA.lexeme,token.COMMA.token,lineNumber);
				else if( c === "%")
					addToken(token.MODULO.lexeme,token.MODULO.token,lineNumber);
				else if( c === "&")
					addToken(token.AMPERSAND.lexeme,token.AMPERSAND.token,lineNumber);
				else if( c === ">"){
						advance();

					if(c === "="){

						addToken(token.GTHANEQUAL_OP.lexeme,token.GTHANEQUAL_OP.token,lineNumber);


					}else{

						addToken(token.GTHAN_OP.lexeme,token.GTHAN_OP.token,lineNumber);
						back();
					}


				}
				else if( c === "<"){
						advance();

					if(c === "="){

						addToken(token.LTHANEQUAL_OP.lexeme,token.LTHANEQUAL_OP.token,lineNumber);


					}else{

						addToken(token.LTHAN_OP.lexeme,token.LTHAN_OP.token,lineNumber);
						back();
					}


				}


			}

			advance();
		}
		else if (isDigit(c)) {
			var num = c;

			while (isDigit(advance())) num += c;
			if (c === ".") {
				do num += c; while (isDigit(advance()));
				//num = parseFloat(num);
				addToken(num,token.NUM_DOUBLE.token,lineNumber);



			}
			else{
				num = parseInt(num);
				addToken(num,token.NUM_INT.token,lineNumber);
			}




		}
		else if (isString(c)) {


		   	addToken(token.DQUOTE.lexeme,token.DQUOTE.token,lineNumber);
			advance();
			var isError= false;
			var str="";
			while (c != '"') {


				if(input[i]==undefined){
					var x = '"';
					addToken(x.concat(str),token.ERROR.token,lineNumber);
					isError = true;
					break;

				}

				else{
					str += c;
				//alert(str);
				advance();

				}

			}



			advance();

			if(!isError){
				addToken(str,token.STRING.token,lineNumber);
				 addToken(token.DQUOTE.lexeme,token.DQUOTE.token,lineNumber);
			}




		}
		else if (isChar(c)) {


		   	addToken(token.SQUOTE.lexeme,token.SQUOTE.token,lineNumber);
			advance();
			var isError= false;
			var str="";
			while (c !="'") {


				if(input[i]==undefined){
					var x = "'";
					addToken(x.concat(str),token.ERROR.token,lineNumber);
					isError = true;
					break;

				}

				else{
					str += c;
				//alert(str);
				advance();

				}

			}



			advance();

			if(!isError){
				addToken(str,token.IS_CHAR.token,lineNumber);
				 addToken(token.SQUOTE.lexeme,token.SQUOTE.token,lineNumber);
			}




		}

		else if (isWord(c)) {

			var word = c;
			//alert(word);
			while (isWord(advance()) && input[i]!=undefined) {

						word  += c;

						//alert(word);
				}

			if(keywords.contains(word)){

				if(word === "main")
					addToken(token.POUCH_C.lexeme,token.POUCH_C.token,lineNumber);
				else if(word === "int"){
					addToken(token.KW_INT.lexeme,token.KW_INT.token,lineNumber);
					dtype = token.KW_INT.lexeme;
				}
				else if (word === "double"){

					addToken(token.KW_DOUBLE.lexeme,token.KW_DOUBLE.token,lineNumber);
					dtype = token.KW_DOUBLE.lexeme;
				}
				else if (word === "char"){

					addToken(token.KW_CHAR.lexeme,token.KW_CHAR.token,lineNumber);
					dtype = token.KW_CHAR.lexeme;
				}

				else if (word === "if")
					addToken(token.KW_IF.lexeme,token.KW_IF.token,lineNumber);
				else if(word === "else")
					addToken(token.KW_ELSE.lexeme,token.KW_ELSE.token,lineNumber);
				else if(word === "while")
					addToken(token.KW_WHILE.lexeme,token.KW_WHILE.token,lineNumber);
				else if(word === "printf")
					addToken(token.PRINTF.lexeme,token.PRINTF.token,lineNumber);
				else if(word === "scanf")
					addToken(token.SCANF.lexeme,token.SCANF.token,lineNumber);


			}

			else{


				addToken(word,token.ID.token,lineNumber);
			}



		}
		else {

			var err ="";
			 //alert(err);
			while (!isWord(c) && !isString(c) && !isDigit(c) && !isSymbol(c) && !isWhiteSpace(c)){

				 err += c;
				 //alert(err);
				 advance();
			}



			addToken(err,token.ERROR.token,lineNumber);



		}

	}
	addToken(token.EOF.lexeme,token.EOF.token,lineNumber);
	return tokens;
};