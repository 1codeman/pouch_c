

var errors=[], symblTbl=[];
var tempInitializationID, tempIDvalue,tempID,tempDtype,val; //global temporaty value Holder
var loopCounter = 0,index=0;;
var found = 0,index=0;
var operand1,operand2;
var stack=[];
var errorManager = "";
var stackValue = 0;
var tempAssignDtype="int",stdinput="";
var i,stdinputCounter=0;
var result = "";
var ifValue=0;
var need="";
var conditionValue = "";
var whileFlag = 0;
var if_flag = 0;
var negative = false;
var parser = function (tokens){
	
	 i=0;
	 found = 0,index=0;
	 tempIDvalue="",tempID="",tempDtype=""; 	
	 stdinputCounter=0;
	 if_flag = 0;
	 
	 var splittedSTDINPUT = stdinput.split(" ");
	try{
		
	errors=[];
    symblTbl=[];

	parseProgram();
  
	 
	 function parseProgram() {
	 		symblTbl=[];
	 		result ="";
	 		expect(token.POUCH_C.token,token.POUCH_C.lexeme);
	 		expect(token.LEFT_PAREN.token,token.LEFT_PAREN.lexeme);
	 		expect(token.RIGHT_PAREN.token,token.RIGHT_PAREN.lexeme);
	 		expect(token.LEFT_BRACE.token,token.LEFT_BRACE.lexeme);
	 	       
			//process stmt_list
			parseStmtList();
		   
			  expect(token.RIGHT_BRACE.token,token.RIGHT_BRACE.lexeme);
			  
			  	
			/*while(tokens[i].lexeme !=  "EOF" && flag === 0){
				var error = "ERROR: Expecting  END OF PROGRAM";
	 			addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);	
	 			i++;
			}*/
				
		}
		
		function parseStmtList() {
			
	 		var flag = 0;
	 		while(tokens[i].lexeme !=  "EOF"){
	 			loopCounter = 0;
	 			stackValue = 0;
	 			switch(tokens[i].token){                           
	 				
	 				case token.KW_INT.token:
	 				case token.KW_DOUBLE.token:
	 				case token.KW_CHAR.token:
	 					//process decl_stmt
	 				
	 				
	 					parseDeclStmt();
	 					break;
	 				case token.ID.token:
	 				    //process ass_stmt          
	 					parseAssignStmt();
	 					break;
	 				case token.PRINTF.token:
	 				
	 				    parsePrintf();
	 				    break;
	 				case token.KW_IF.token:
	 				   
	 				    parseIf();
	 				    break;
	 			   case token.SCANF.token:
	 				   parseScanf();
	 				    break;
	 			   case token.KW_WHILE.token:
	 				    parseWhile();
	 				    break;
	 				default:
	 					flag = 1;
	 					
	 				
	 			}   
	 			
	 			if(if_flag === 1)
	 			break;
	 			
	 			if_flag  =  0;
	 			
	 			if(flag === 1 )
	 			 break; //break the while loop
		
	 			
	 		}
	
		 	
		}
		
		
		function parseScanf(){
		
			
			expect(token.SCANF.token,token.SCANF.lexeme);
			expect(token.LEFT_PAREN.token, token.LEFT_PAREN.lexeme);
			expect(token.DQUOTE.token, token.DQUOTE.lexeme);
			if(tokens[i].token === token.STRING.token){
				
				if(tokens[i].lexeme === "%d"){
				    need = "int";
				    
				}
				else if(tokens[i].lexeme === "%ld"){
					need = "double";
				}
				else if(tokens[i].lexeme === "%c"){
					need = "char";
				}
				else{
						var error = "ERROR: expecting %d or %ld";
	 				addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);
				}
				
				
				
				
			}
			
			expect(token.STRING.token,tokens[i].lexeme);
			expect(token.DQUOTE.token, token.DQUOTE.lexeme);
			expect(token.COMMA.token, token.COMMA.lexeme);
			expect(token.AMPERSAND.token, token.AMPERSAND.lexeme);
			
			  
	 	  	  if(tokens[i].token === token.ID.token){
				 if(searchSymbol(tokens[i].lexeme) ===false){

				 	var error = "ERROR: ID '"+ tokens[i].lexeme + "' is not declared!";
	 				addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);	
				 }else{
				 	
				 
				tempID = tokens[i-1].lexeme;
				searchSymbol(tempID);
				tempAssignDtype  = tempDtype;
				tempInitializationID = index;
				
				
				 }
				 
				 if(need != tempDtype){
				 	if(need === "double"){
				 		var error = "ERROR: %ld not expecting " + tempDtype;
	 				addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);	
				 		
				 	}
				 	else if(need === "char"){
				 		var error = "ERROR: %c not expecting " + tempDtype;
	 				addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);	
				 		
				 	}
				 	else{
				 		var error = "ERROR: %d not expecting " + tempDtype;
	 				addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);	
				 		
				 	}
				 	
				 }				 
	 		     
				 
			   }
         	expect(token.ID.token, tokens[i].lexeme);
         	expect(token.RIGHT_PAREN.token, token.RIGHT_PAREN.lexeme);
         	expect(token.SEMICOLON.token,token.SEMICOLON.lexeme);	
         	      
         	
         		
         	   if(errors.length == 0 ) {//if theres no syntax error update symbol table
                 
                 if(tempDtype === "char"){
                 	  var c = splittedSTDINPUT[stdinputCounter];
                 	   	if(typeof splittedSTDINPUT[stdinputCounter]=== 'undefined'){
  										var error = "ERROR: expecting a value in stdin";
	 							addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);
 					}else{
 							updateSymbol(c[0],tempInitializationID);
                      stdinputCounter++;
 						
 					}
                 	  
                 }else{
                 	   if(isNaN(splittedSTDINPUT[stdinputCounter])){
                 	   	var error = "ERROR: expecting a number in stdin";
	 				addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);	
                 	   }else{
                 	   	
                 	   	if(typeof splittedSTDINPUT[stdinputCounter]=== 'undefined'){
  										var error = "ERROR: expecting a value in stdin";
	 							addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);
 					}else{
 						
 						updateSymbol(splittedSTDINPUT[stdinputCounter],tempInitializationID);
                        stdinputCounter++;
 						
 					}
                 	   
                 	   }
                 	  
                 }
                  
               
               }		
			
			
		}
		function parseWhile (){
			whileFlag = 0;
			
			while(whileFlag === 0){
				
					
			var whileCounter= i;
			expect(token.KW_WHILE.token,token.KW_WHILE.lexeme);
			expect(token.LEFT_PAREN.token,token.LEFT_PAREN.lexeme);
			parseE();
	
			
				if(conditionValue === false){
				
			       whileFlag = 1;
				    
			
			}
			expect(token.RIGHT_PAREN.token,token.RIGHT_PAREN.lexeme);
			expect(token.LEFT_BRACE.token,token.LEFT_BRACE.lexeme);
			if(whileFlag === 1){
				while(tokens[i].lexeme != "}"){
					i++;
				}
			}else{
				
				parseStmtList();
			}
			
			expect(token.RIGHT_BRACE.token,token.RIGHT_BRACE.lexeme);
		
				
	
			if(errors.length != 0)
				  break;
		   if(whileFlag!=1)
		     i = whileCounter;
		}
			
		}
		function parseIf(){
			expect(token.KW_IF.token,token.KW_IF.lexeme);
			expect(token.LEFT_PAREN.token,token.LEFT_PAREN.lexeme);
			parseE();
			expect(token.RIGHT_PAREN.token,token.RIGHT_PAREN.lexeme);
			
			if(tokens[i].token === token.LEFT_BRACE.token){
				
				expect(token.LEFT_BRACE.token,token.LEFT_BRACE.lexeme);
			
			var elseFlag = 0;
			if(conditionValue === true){
		
				parseStmtList();
				
			}else{
				elseFlag =1;
				while(tokens[i].token != token.RIGHT_BRACE.token)
				   i++;
				//do nothing
				
			}
			
			
			expect(token.RIGHT_BRACE.token,token.RIGHT_BRACE.lexeme);
			
			if(tokens[i].lexeme === token.KW_ELSE.lexeme){
				expect(token.KW_ELSE.token,token.KW_ELSE.lexeme);
				
				if(tokens[i].token === token.LEFT_BRACE.token){
					expect(token.LEFT_BRACE.token,token.LEFT_BRACE.lexeme);
				
				
				if(elseFlag === 1){
					parseStmtList();
				
			}else{
				
				while(tokens[i].token != token.RIGHT_BRACE.token)
				   i++;
				//do nothing
				
				}
				
				expect(token.RIGHT_BRACE.token,token.RIGHT_BRACE.lexeme);
					
					
				}else{
					if(elseFlag === 1){
					parseStmtList();
				
			}else{
				
				while(tokens[i-1].token != token.SEMICOLON.token)
				   i++;
				//do nothing
				
				}
				}
				
				
			}
			
			}else{
				
				
				elseFlag = 0;
				if(conditionValue === true){
		
				parseStmtList();
				
			}else{
				elseFlag =1;
				while(tokens[i-1].token != token.SEMICOLON.token)
				   i++;
				//do nothing
				
			}
			
			if(token.KW_ELSE.token === tokens[i].token){
			
				expect(token.KW_ELSE.token,token.KW_ELSE.lexeme);
				if(tokens[i].token === token.LEFT_BRACE.token){
			
				
				expect(token.LEFT_BRACE.token,token.LEFT_BRACE.lexeme);
				
				
				if(elseFlag === 1){
					parseStmtList();
				
			}else{
				
				while(tokens[i].token != token.RIGHT_BRACE.token)
				   i++;
				//do nothing
				
				}
				
				expect(token.RIGHT_BRACE.token,token.RIGHT_BRACE.lexeme);
				
			
				
			}
				
			else{
				
				
				if(elseFlag === 1){
					parseStmtList();
				
			}else{
				
				while(tokens[i-1].token != token.SEMICOLON.token)
				   i++;
				//do nothing
				
				}
				
				
			}
			
			}
			
			}
			
			
			
		}
		
		function parsePrintf() {
			
			expect(token.PRINTF.token,token.PRINTF.lexeme);
			expect(token.LEFT_PAREN.token,token.LEFT_PAREN.lexeme);
			
			expect(token.DQUOTE.token,token.DQUOTE.lexeme);
			
			if(expect(token.STRING.token,tokens[i].lexeme)=== true){
				    var n = tokens[i-1].lexeme.search("%d");
					var str = tokens[i-1].lexeme;
				    //alert(n);
				    if(n > -1){
				    			
		            	expect(token.DQUOTE.token,token.DQUOTE.lexeme);
				    	expect(token.COMMA.token,token.COMMA.lexeme);
				    	expect(token.ID.token,token.ID.lexeme);
			
						if(searchSymbol(tokens[i-1].lexeme)===true){
					        if(symblTbl[index].type === "int"){
					        	var res = str.replace("%d", symblTbl[index].value);
							     result+= res + "\n";
					        }
					        else if(symblTbl[index].type === "char"){
					        	
					        	var res = str.replace("%d", symblTbl[index].value.charCodeAt(0));
							     result+= res + "\n";
					        }
					        else{
					        	
					        	var error = "ERROR: %d expecting an integer value";
	 					         addError(tokens[i-1].lexeme,tokens[i-1].token,tokens[i-1].lineNumber,error);
					        	
					        }
				    	    
						}
					else{
						var error = "ERROR: Id you want to print is not declared";
	 					addError(tokens[i-1].lexeme,tokens[i-1].token,tokens[i-1].lineNumber,error);
					
					}
				    	
				    	
				    }else{
				    	
				    	n = tokens[i-1].lexeme.search("%ld");
				    	
				    	if(n > -1){
				    		
				    	expect(token.DQUOTE.token,token.DQUOTE.lexeme);
				    	expect(token.COMMA.token,token.COMMA.lexeme);
				    	expect(token.ID.token,token.ID.lexeme);
			
						if(searchSymbol(tokens[i-1].lexeme)===true){
					        if(symblTbl[index].type === "double"){
					        	var res = str.replace("%ld", parseFloat(symblTbl[index].value).toFixed(2));
							     result+= res + "\n";
					        }
					        else{
					        	
					        	var error = "ERROR: %ld expecting a double type value";
	 					         addError(tokens[i-1].lexeme,tokens[i-1].token,tokens[i-1].lineNumber,error);
					        	
					        }
				    	    
						}
					else{
						var error = "ERROR: Id you want to print is not declared";
	 					addError(tokens[i-1].lexeme,tokens[i-1].token,tokens[i-1].lineNumber,error);
					
					}
				    	
				    	}else{
				    		n = tokens[i-1].lexeme.search("%c");
				    	
				    	if(n > -1){
				    		
				    	expect(token.DQUOTE.token,token.DQUOTE.lexeme);
				    	expect(token.COMMA.token,token.COMMA.lexeme);
				    	expect(token.ID.token,token.ID.lexeme);
			
						if(searchSymbol(tokens[i-1].lexeme)===true){
					        if(symblTbl[index].type === "char"){
					        	var res = str.replace("%c", symblTbl[index].value);
							     result+= res + "\n";
					        }
					        else{
					        	
					        	var error = "ERROR: %ld expecting a char type value";
	 					         addError(tokens[i-1].lexeme,tokens[i-1].token,tokens[i-1].lineNumber,error);
					        	
					        }
				    	    
						}
					else{
						var error = "ERROR: Id you want to print is not declared";
	 					addError(tokens[i-1].lexeme,tokens[i-1].token,tokens[i-1].lineNumber,error);
					
					}
				    	
				    	}else{
				    		result+= tokens[i-1].lexeme + "\n";
				    		expect(token.DQUOTE.token,token.DQUOTE.lexeme);
				    		
				    	}
				    		
				    	}
				    }
					
				}

			/*if(token.ID.token === tokens[i].token){
				expect(token.ID.token,token.ID.lexeme);
			
				if(searchSymbol(tokens[i-1].lexeme)===true){
					result+= symblTbl[index].value + "\n";
				}
				else{
					var error = "ERROR: Id you want to print is not declared";
	 			addError(tokens[i-1].lexeme,tokens[i-1].token,tokens[i-1].lineNumber,error);
					
				}
				
			}else if(token.DQUOTE.token === tokens[i].token){
		
				expect(token.DQUOTE.token,token.DQUOTE.lexeme);
				
				if(expect(token.STRING.token,tokens[i].lexeme)=== true){
					result+= tokens[i-1].lexeme + "\n";
				}
				
				expect(token.DQUOTE.token,token.DQUOTE.lexeme);
				
			}*/
			
			expect(token.RIGHT_PAREN.token,token.RIGHT_PAREN.lexeme);
			expect(token.SEMICOLON.token,token.SEMICOLON.lexeme);
		}
		
		function parseAssignStmt() {
			
			  tempIDvalue = "";
			  tempID="";
			  
	 
	
	 	  	  
	 	  	  if(tokens[i].token === token.ID.token){
				 if(searchSymbol(tokens[i].lexeme) ===false){
			
				 	var error = "ERROR: ID '"+ tokens[i].lexeme + "' is not declared!";
	 				addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);	
				 }else{
				 	
				 
				tempID = tokens[i-1].lexeme;
				searchSymbol(tempID);
				tempAssignDtype  = tempDtype;
				tempInitializationID = index;
				 }
				 
	 		     
				 
			   }
			   
			   
			   
			   	expect(token.ID.token,tokens[i].lexeme);
				 
			   
				
	           if(expect(token.ASSIGN_OP.token,token.ASSIGN_OP.lexeme) == true) {
	            if(tempAssignDtype === "char"){
	            		
	 				if(tokens[i].token === token.SQUOTE.token){
	 						expect(token.SQUOTE.token,token.SQUOTE.lexeme);
	 						if(tokens[i].token === token.IS_CHAR.token){
	 							var c = tokens[i].lexeme;
	 								expect(token.IS_CHAR.token,token.IS_CHAR.lexeme);
	 							if(errors.length == 0 ) {//if theres no syntax error update symbol table

               	            	 // alert("char = " + c[0]);
		                 			updateSymbol(c[0],tempInitializationID);
		           				}
	 						}
	 							expect(token.SQUOTE.token,token.SQUOTE.lexeme);
	 							 
	 					
	 				}
	 					
	            	
	            }else{
	            	
	            	if(tokens[i].lexeme == '-'){
	 					negative = true;
	 					i++;
	 				}
	 				    
	            	//process parseE()
	           	 
	           	 var aflag = 0;
	 	    	  while(tokens[i].lexeme !=  ";"){
	 	    	  	aflag = 1;
	 	    	  	parseE();
	 	    	  }
	 	    	  
	 	    	  if(aflag===0){
	 	    	  	var error = "ERROR: Expression is needed in Assignment statement";
	 				addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);	
	 	    	  	i++;
	 	    	  }
	            }
	           	 
              	 	
              
	           }
	           else{
	           	i++;
	           }
	 	
	 	
	 	     
	 	       expect(token.SEMICOLON.token,token.SEMICOLON.lexeme);	
             
               //update symbol table
              if(errors.length == 0 ) {//if theres no syntax error update symbol table
                 
                  
                 updateSymbol(stackValue,tempInitializationID);
                 
               }
             	                
		}
		                                                                           
		
		function parseE() {

			parseT();
			parseERest();
			
		}
		
		function parseT() {
			parseQ();
			parseTRest();
		}
		
		function parseQ(){
			parseF();
			parseQRest();
		}
		
		function parseQRest(){
			if(tokens[i].token === token.MULT_OP.token){
					expect(token.MULT_OP.token,token.MULT_OP.lexeme);
				
					parseF();
					parseQRest();
					operand1 = stack.pop();
					operand2 = stack.pop();
					toEvaluate(operand1,operand2,"*");
					
				}else if(tokens[i].token === token.DIV_OP.token){
					expect(token.DIV_OP.token,token.DIV_OP.lexeme);
					
					parseF();
					parseQRest();
					
					operand1 = stack.pop();
					operand2 = stack.pop();
					toEvaluate(operand1,operand2,"/");
					
				}else if(tokens[i].token === token.MODULO.token){
					expect(token.MODULO.token,token.MODULO.lexeme);
					
					parseF();
					parseQRest();
					
					operand1 = stack.pop();
					operand2 = stack.pop();
					toEvaluate(operand1,operand2,"%");
					
				}
				else{
					//do nothing
				}
			
		}
		
		function parseERest() {
			
				if(tokens[i].token === token.EQUALITY_OP.token){
				
					expect(token.EQUALITY_OP.token,token.EQUALITY_OP.lexeme);
					parseT();
					parseERest();
					
					operand1 = stack.pop();
					operand2 = stack.pop();
					toEvaluate(operand1,operand2,"==");
					
				}else if(tokens[i].token === token.GTHAN_OP.token){
					expect(token.GTHAN_OP.token,token.GTHAN_OP.lexeme);
					
					parseT();
					parseERest();
					
					operand1 = stack.pop();
					operand2 = stack.pop();
					toEvaluate(operand1,operand2,">");
					
				}else if(tokens[i].token === token.LTHAN_OP.token){
					expect(token.LTHAN_OP.token,token.LTHAN_OP.lexeme);
					
					parseT();
					parseERest();
					
					operand1 = stack.pop();
					operand2 = stack.pop();
					toEvaluate(operand1,operand2,"<");
					
				}else if(tokens[i].token === token.GTHANEQUAL_OP.token){
					expect(token.GTHANEQUAL_OP.token,token.GTHANEQUAL_OP.lexeme);
					
					parseT();
					parseERest();
					
					operand1 = stack.pop();
					operand2 = stack.pop();
					toEvaluate(operand1,operand2,">=");
					
				}else if(tokens[i].token === token.LTHANEQUAL_OP.token){
					expect(token.LTHANEQUAL_OP.token,token.LTHANEQUAL_OP.lexeme);
					
					parseT();
					parseERest();
					
					operand1 = stack.pop();
					operand2 = stack.pop();
					toEvaluate(operand1,operand2,"<=");
					
				}else{
					//do nothing	
				}
			
				

		}
		
		function parseTRest() {
			
			if(tokens[i].token === token.PLUS_OP.token){
					expect(token.PLUS_OP.token,token.PLUS_OP.lexeme);
				
					parseQ();
					parseTRest();
					
					operand1 = stack.pop();
					operand2 = stack.pop();
					toEvaluate(operand1,operand2,"+");
					
				}else if(tokens[i].token === token.MINUS_OP.token){
					expect(token.MINUS_OP.token,token.MINUS_OP.lexeme);
					
					parseQ();
					parseTRest();
					
					operand1 = stack.pop();
					operand2 = stack.pop();
					toEvaluate(operand1,operand2,"-");
					
				}else{
					//do nothing	
				}
			
				
		}
		
		function parseF() {
             //alert("token = "+ tokens[i-1].lexeme );
			//check if  i-1 is valid op
			if(tokens[i-1].token === token.ID.token || tokens[i-1].token === token.NUM_DOUBLE.token || tokens[i-1].token === token.NUM_INT.token){
				var error = "ERROR: INVALID EXPRESSION a";
	 			addError(tokens[i-1].lexeme,tokens[i-1].token,tokens[i-1].lineNumber,error);	
			}
			
			if(tokens[i].token === token.ID.token){
				 if(searchSymbol(tokens[i].lexeme) ===false){
			
				 	var error = "ERROR: ID '"+ tokens[i].lexeme + "' is not declared!";
	 				addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);	
				 }
				 
				 searchSymbol(tokens[i-1].lexeme);
				 stackValue = symblTbl[index].value;
				  pushStack(symblTbl[index].type,symblTbl[index].value);
	
				 expect(token.ID.token,token.ID.lexeme);
				 
				 
			}else if(tokens[i].token === token.NUM_INT.token || tokens[i].token === token.NUM_DOUBLE.token){
			    if(negative === true){
			    	//alert("mnega"); 
			    	tokens[i].lexeme = tokens[i].lexeme *-1;
			    	negative =false;
			    }
			     
				if(tokens[i].token === token.NUM_INT.token){
					 expect(token.NUM_INT.token,token.NUM_INT.lexeme);
					 check("int");
					  stackValue = tokens[i-1].lexeme;
					 pushStack("int",tokens[i-1].lexeme);
				}else{
					expect(token.NUM_DOUBLE.token,token.NUM_DOUBLE.lexeme);
					check("double");
					 stackValue = tokens[i-1].lexeme;
					 pushStack("double",tokens[i-1].lexeme);
					 
				}	
				
			}else if(tokens[i].token === token.LEFT_PAREN.token){
	
				expect(token.LEFT_PAREN.token,token.LEFT_PAREN.token);
				
				if(tokens[i].lexeme === '-'){
					negative = true;
					i++;
				}
				while(tokens[i].token!= token.RIGHT_PAREN.token){
					parseE();
				}
			
				expect(token.RIGHT_PAREN.token, token.RIGHT_PAREN.lexeme);
				
				if(tokens[i].lexeme != "+" && tokens[i].lexeme != "-"  && tokens[i].lexeme != "*" && tokens[i].lexeme != "/" && tokens[i].lexeme != "*" && tokens[i].lexeme != ";"){
					var error = "ERROR: EXPRESSION IS NOT VALID a";
	 					addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);	
					
				}
				
			}else{
						var error = "ERROR: EXPRESSION IS NOT VALID b";
	 					addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);	
						 i++;
						
						    if(tokens[i-1].lexeme === ";" || tokens[i-1].lexeme === ")" ){
							i--;
						    if( loopCounter++ > 50){
						    	i++;
						    }
						    	
				          }		
			}
			
		
		}// end parseF
	
	function parseDeclStmt() {
	 	
	 		if(tokens[i].token === token.KW_INT.token){
	 			i++;
	 			//process ID_LIST
	 			parseIDList(token.KW_INT.lexeme);
	 			
	 		}else if(tokens[i].token === token.KW_DOUBLE.token){
	 			
	 	
	 			i++;
	 			parseIDList(token.KW_DOUBLE.lexeme);
	 			
	 		}	   else{
	 			
	 			i++;
	 			parseIDList(token.KW_CHAR.lexeme);
	 			
	 		} 
		
		}
		 
		function parseIDList(dtype) {
			
	 		if( expect(token.ID.token,"ID")){
	 			  
	 				if(symblTbl.length === 0){
	 					insertSymbol(dtype,tokens[i-1].lexeme,0);
	 				}
	 				else if(searchSymbol(tokens[i-1].lexeme) ===false){
	 					insertSymbol(dtype,tokens[i-1].lexeme,0);
	 				}
	 				else{
	 					var error = "ERROR: ID already existed!!";
	 					addError(tokens[i-1].lexeme,tokens[i-1].token,tokens[i-1].lineNumber,error);	
	 				}
	 				
	 			}
	 			
	 			
	 			//for initialization
	 			if(tokens[i].token === token.ASSIGN_OP.token) {
	 				if(dtype === "char"){
	 					 tempID="";
			        //hello
			        
	 					tempID = tokens[i-1].lexeme;
						searchSymbol(tempID);
						tempAssignDtype  = tempDtype;
						tempInitializationID = index;
	 				
	 				expect(token.ASSIGN_OP.token,token.ASSIGN_OP.lexeme);
	 				
	 				if(tokens[i].token === token.SQUOTE.token){
	 						expect(token.SQUOTE.token,token.SQUOTE.lexeme);
	 						if(tokens[i].token === token.IS_CHAR.token){
	 							var c = tokens[i].lexeme;
	 								expect(token.IS_CHAR.token,token.IS_CHAR.lexeme);
	 							if(errors.length == 0 ) {//if theres no syntax error update symbol table

               	            	  //alert("char = " + c[0]);
		                 			updateSymbol(c[0],tempInitializationID);
		           				}
	 						}
	 							expect(token.SQUOTE.token,token.SQUOTE.lexeme);
	 							 
	 					
	 				}
	 					
	 				}else{
	 					
	 					  tempID="";
			        //hello
			        
	 					tempID = tokens[i-1].lexeme;
						searchSymbol(tempID);
						tempAssignDtype  = tempDtype;
						tempInitializationID = index;
	 				
	 				expect(token.ASSIGN_OP.token,token.ASSIGN_OP.lexeme);
	 				//process parseE
	 				
	 				if(tokens[i].lexeme == '-'){
	 					negative = true;
	 					i++;
	 				}
	 				    
	 				var eflag = 0;
	 				while(tokens[i].lexeme != ";"){
	 					eflag = 1;
	 				   parseE();
	 				}
	 		       
	 		       if(eflag === 0){
	 		       	var error = "ERROR: expression needed in declaration!";
	 					addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);	
	 					i++;
	 		       	
	 		       }
	 				
	 			//update symbol table
              	 	if(errors.length == 0 ) {//if theres no syntax error update symbol table

               	            var x= stack.pop();
               	         
               	      
		                 	updateSymbol(x.value,tempInitializationID);
		           }
             
	 				
	 		}
	 		
	 		
	 }
	 // for multi declaration
	 		while(tokens[i].token === token.COMMA.token){
	 			   i++;
	 			if(expect(token.ID.token,"ID")){
	 				if(searchSymbol(tokens[i-1].lexeme) ===false){
	 					insertSymbol(dtype,tokens[i-1].lexeme,0.0);
	 				}
	 				else{
	 					var error = "ERROR: ID already existed!!";
	 					addError(tokens[i-1].lexeme,tokens[i-1].token,tokens[i-1].lineNumber,error);	
	 				}
	 				
	 			}
	 		
	 		}
			  expect(token.SEMICOLON.token,token.SEMICOLON.lexeme);
	 	
			      
			  
		}
	
	 function expect(tok,lex) {
          
          
	 	  var token = tokens[i].token;
	 
	      if (typeof token === undefined){
	 
	      	token = "";
	      	
	      }
	          
	          
	 		if(token === tok){
	 			i++;
	 			return true;
	 			
	 		}else{
	 			
	 		
	 			// save error to error table
	 			var error = "ERROR: Expecting  '" + lex + "' but instead gets '"+ tokens[i].lexeme +"'.";
	 			addError(tokens[i].lexeme,tokens[i].token,tokens[i].lineNumber,error);	
	 			i++;
	 		   	return false;	
	 		}  
	
	}
	
			
	
		if(errors.length ===0){
			return "SYNTAX ACCEPTED";
		}else{
			
			return errors;
		}
		
	
	}catch (e){
		
		     
		
			throw errors[0].error + " on line : " +errors[0].lineNumber;
		
	  
		
	}
	

	
	
};
