var start;
var end;
angular.module('starter.controllers', [])

.controller('SettingsCtrl', function ($scope, $location, $window) {
 
        $scope.themes = [
            'light',
            'stable',
            'positive',
            'calm',
            'balanced',
            'energized',
            'assertive',
            'royal',
            'dark'
        ];
 
        var selectedTheme = $window.localStorage.appTheme;
        if (selectedTheme) {
            $scope.appTheme = selectedTheme;
        } else {
            $scope.appTheme = 'positive';
        }	
 
        $scope.themeChange = function (theme) {
            // save theme locally
            $window.localStorage.appTheme = theme;
            // reload
            $window.location = '';
        }
 
    })

.controller('DashCtrl', function($scope,Codes) {
	
       
	 $scope.setDefaultCode = function() {


     	
      $scope.dash =Codes.setCode("main(){\n\n\n\n\n\n\n}");
   	
   	
  };
})


.controller('StartCodingCtrl', function($scope,Codes) {
	
   $scope.code = Codes.getCode();
  
   $scope.generate = function() {
   	
     start = performance.now();
   	
   	var input = document.getElementById("input").value;
   	stdinput = document.getElementById("stdinput").value;
   	
		var output = function1(input);
	end = performance.now();
    	var time = end - start;
		if(errors.length === 0 ){
			document.getElementById("output").innerHTML = result;
			document.getElementById("output").style.color = "black";
			 document.getElementById("time").innerHTML = "success |"+ "run time: " + time.toFixed(2) + "ms" ;
			 document.getElementById("time").style.backgroundColor = "#91e374";
		}
		
		else{
			document.getElementById("output").innerHTML = output;
			document.getElementById("output").style.color = "#e52325";
            document.getElementById("time").innerHTML = "translation error: there are ("+ errors.length + ") error(s)";
            document.getElementById("time").style.backgroundColor = "#e52325";
		}
    	
    	
    	
    	
      
  };
  
  
   var function1 = function(input){
  	
  	try {
			
		
			var tokens = lex(input);
			var errors = parser(tokens);
			var myObject = JSON.stringify(errors,null,"\t");
			myObject = JSON.stringify(errors,null,4);
		
	
			return myObject;
		} catch (e) {
			return e;
		}
  };
  
  
  $scope.insertAtCaret=function(areaId,text) {

	var txtarea = document.getElementById(areaId);
	var scrollPos = txtarea.scrollTop;
	var strPos = 0;
	var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? 
		"ff" : (document.selection ? "ie" : false ) );
	if (br == "ie") { 
		txtarea.focus();
		var range = document.selection.createRange();
		range.moveStart ('character', -txtarea.value.length);
		strPos = range.text.length;
	}
	else if (br == "ff") strPos = txtarea.selectionStart;
	
	var front = (txtarea.value).substring(0,strPos);  
	var back = (txtarea.value).substring(strPos,txtarea.value.length); 
	txtarea.value=front+text+back;
	strPos = strPos + text.length;
	if (br == "ie") { 
		txtarea.focus();
		var range = document.selection.createRange();
		range.moveStart ('character', -txtarea.value.length);
		range.moveStart ('character', strPos);
		range.moveEnd ('character', 0);
		range.select();
	}
	else if (br == "ff") {
		txtarea.selectionStart = strPos;
		txtarea.selectionEnd = strPos;
		txtarea.focus();
	}
	txtarea.scrollTop = scrollPos;
};
  
  
})

.controller('LearnPouchCtrl', function($scope, Codes) {
  
  
        
     $scope.setCode1 = function() {

    var input = "main(){\n int x,y;\n x = 0; \ny = 3; \n printf(\"x = %d\",x);\n\nprintf(\"y = %d\",y);\n}";

     	
      $scope.learnpouch =Codes.setCode(input);
   	
   	
  };
  
  
  $scope.setCode2 = function() {

    var input = "main(){\n int x = 0; \n double y = 10.4;\n printf(\"x = %d\",x);\n\nprintf(\"y = %ld\",y);\n}";

     	
      $scope.learnpouch =Codes.setCode(input);
   	
   	
  };
  
   $scope.setCode3 = function() {

  var input = "main(){\n int x,y;\n x = 3*2+(2-1); \n y = x*2-1; \n printf(\"x = %d\",x);\n\nprintf(\"y = %d\",y);\n}";

     	
      $scope.learnpouch =Codes.setCode(input);
   	
   	
  };
  
     $scope.setCode4 = function() {

  var input = "main(){\nint a = 50;\n if(a<20){\nprintf(\"a is less than 20\");\n}else{\nprintf(\"a is not less than 20\");\n}\nprintf(\"the value of a is: %d\", a);\n}";
  
     	
      $scope.learnpouch =Codes.setCode(input);
   	
   	
  };
  
  



 $scope.setCode5 = function() {

  var input = "main ()\n{\nint a = 10;\nwhile( a < 20 )\n{\nprintf(\"value of a: %d\", a);\na= a+1;\n}\n}\n";

     	
      $scope.learnpouch =Codes.setCode(input);
   	
   	
  };

  $scope.setCode6 = function() {

  var input = "main(){\nint x =100;\ndouble y = 14.2;\nchar z = 'A';\nprintf(\"x = %d\",x);\nprintf(\"y = %ld\",y);\nprintf(\"z = %c\",z);\nprintf(\"ascii value of 'A' = %d\",z);\n}\n";

     	
      $scope.learnpouch =Codes.setCode(input);

  };
  
  
  $scope.setCode7 = function() {

  var input = "main(){\nint x;\ndouble y;\nchar z;\nscanf(\"%d\",&x);\nscanf(\"%ld\",&y);\nscanf(\"%c\",&z);\nprintf(\"x = %d\",x);\nprintf(\"y = %ld\",y);\nprintf(\"z = %c\",z);\n}";

     	
      $scope.learnpouch =Codes.setCode(input);
   	
   	
  };
  
  })


